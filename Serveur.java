/**
 * Created by m.najafi on 10/18/2017.
 */
Exercice 4.2:
        import java.util.Vector;

public class Serveur {
    Vector<Client> client_connecter;
    public Serveur(){

        client_connecter = new Vector<Client>();
    }
    public boolean connecter(Client name){
        client_connecter.add(name);
        return true;

    }

    public  void diffuser(String message){
        for(int i =0;i<client_connecter.size();i++) {
            client_connecter.elementAt(i).recois(message);


        }
    }
}

public class Client {
    public String nom;
    private Serveur serveur;
    String messages;
    public Client(String nom) {
        this.nom = nom;

    }
    public boolean se_connecter(Serveur serveur){
        this.serveur = serveur;
        return serveur.connecter(this);
    }
    public void envoyer(String message){
        serveur.diffuser(message);

    }

    public String recois(String message){
        messages = message;
        return messages;
    }

}

}


